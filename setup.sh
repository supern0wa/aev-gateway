# Resource Server API
# Frontend API
# Admin API
# Auth API

# OAuth Plugin
curl -X POST http://kong:8001/apis/{api}/plugins \
	--data "name=oauth2" \
	--data "config.enable_authorization_code=true" \
	--data "config.scopes=email,phone,address" \
	--data "config.mandatory_scope=true"

# CORs Plugin
curl -X POST http://kong:8001/plugins \
	--data "name=cors" \
	--data "config.origins=*" \
	--data "config.exposed_headers=Origin, Authorization" \
	--data "config.max_age=3600"

# Get Provision Key
# TODO: curl above returns plugin data

# Create Consumer
curl -X POST http://kong:8001/consumers/ \
	--data "username=user123" \
	--data "custom_id=SOME_CUSTOM_ID"

# Create Application
curl -X POST http://kong:8001/consumers/{consumer_id}/oauth2 \
	--data "name=Test%20Application" \
	--data "client_id=SOME-CLIENT-ID" \
	--data "client_secret=SOME-CLIENT-SECRET" \
	--data "redirect_uri=http://some-domain/endpoint/"

# Generate certificates
openssl req -newkey rsa:2048 -nodes -keyout aev.dev.key -x509 -days 365 -out aev.dev.pem

curl -i -X POST http://localhost:8001/certificates \
	-F "cert=@./aev.dev.pem" \
	-F "key=@./aev.dev.key" \
	-F "snis=api.aev.dev,moon.aev.dev,admin.aev.dev,aev.dev"

# Delete certificates
rm aev.dev.key
rm aev.dev.pem
